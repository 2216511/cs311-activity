import java.util.Arrays;


public class PairProgramming {

    public static int points(String[] games) {
        int x = 0;
        for (String game : games) {
            String[] arr = game.split(":");
            int first = Integer.parseInt(arr[0]);
            if (Integer.parseInt(arr[0]) > Integer.parseInt(arr[1])) {
                x += 3;
            } else if (Integer.parseInt(arr[0]) < Integer.parseInt(arr[1])) {
                x += 0;
            } else {
                x += 1;
            }
        }
        return x;
    }

    public static int amountOfPages(int summary) {
        if (summary <= 9) {return summary;}
        int count = 0;
        int res = 0;
        for (int i = 1; i < summary; i++){
            count += Integer.toString(i).length();
            if (count == summary) res = i;
        }
        return res;
    }

    public static String ascendDescend(int length, int minimum, int maximum) {
        StringBuilder num = new StringBuilder();
        int y = minimum;
        int z = maximum - 1;
        for (int x = 0; x < length; x++){
            if (y <= maximum){
                num.append(y++);
                if (y == maximum)
                    z = maximum - 1;
            } else {
                num.append(z--);
                if (z == minimum)
                    y = minimum;
            }
        }
        return num.toString();
    }

    public static boolean smallEnough(int[] a, int limit)
    {
        int out = 0;
        for (int i : a) {
            if (i <= limit) {
                out++;
            }
        }
        return out == a.length;
    }

    public class DRoot { //6 kyu
        public static int digital_root(int n) {
            int sum=0;
            String str=String.valueOf(n);
            for(int i=0;i<str.length();i++){
                int a=Character.getNumericValue(str.charAt(i));
                sum+=a;
            }

            if(sum > 9) {
                return digital_root(sum);
            } else {
                return sum;
            }
        }

    }

    public class FindOutlier{ //6th kyu
        static int find(int[] integers){
            int odd = 0;
            for (int n : integers) {
                if (n % 2 != 0) {
                    odd += 1;
                }
            }
            if (odd == 1) {
                for (int n : integers) {
                    if (n % 2 != 0) {
                        return n;
                    }
                }
            } else {
                for (int n : integers) {
                    if (n % 2 == 0) {
                        return n;
                    }
                }
            }
            return 0;
        }
    }

    public class Vowels { //7th kyu

        public static int getCount(String str) {

            int count = 0;

            for (int i=0 ; i<str.length(); i++){
                char ch = str.charAt(i);
                if(ch == 'a'|| ch == 'e'|| ch == 'i' ||ch == 'o' ||ch == 'u'){
                    count ++;
                }
            }
            return count;
        }
    }

    public class isogram { //7th kyu
        public static boolean  isIsogram(String str) {
            str = str.toLowerCase();
            int len = str.length();

            char arr[] = str.toCharArray();

            Arrays.sort(arr);
            for (int i = 0; i < len - 1; i++){
                if (arr[i] == arr[i + 1])
                    return false;
            }
            return true;
        }
    }

    public class Troll { //7th kyu
        public static String disemvowel(String str) {
            str = str.replaceAll("[AEIOUaeiou]","");
            return str;
        }
    }

    public class EvenNumbers { //8th kyu
        public static int[] divisibleBy(int[] numbers, int divider) {

            return Arrays.stream(numbers)
                    .filter(i -> (i % divider) == 0)
                    .toArray();

        }
    }

    public class Multiply { //8th kyu
        public static Double multiply(Double a, Double b) {
            return a * b;
        }
    }







}
